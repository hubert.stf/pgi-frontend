// Imports
import Vue from 'vue'
import Router from 'vue-router'
import { trailingSlash } from '@/util/helpers'
import {
  layout,
  route,
  page,
  isGoingToAnAllowedRoute,
} from '@/util/routes'
import store from '@/store'
import { getCurrentUser as getCurrentUserFromLocalStorage } from '@/services/local-storage'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior: (to, from, savedPosition) => {
    if (to.hash) return { selector: to.hash }
    if (savedPosition) return savedPosition

    return { x: 0, y: 0 }
  },
  routes: [
    page('LoginPage', 'login'),
    page('RegisterPage', 'register'),
    layout('Default', [
      route('Dashboard'),
      route('Incomes', null, 'incomes'),
      route('Expenses', null, 'expenses'),
      route('Periods', null, 'settings/periods'),
      route('Categories', null, 'settings/categories'),
      route('PaymentMethods', null, 'settings/payment-methods'),
      route('PaymentRecurrences', null, 'settings/payment-recurrences'),
      route('PaymentStatuses', null, 'settings/payment-statuses'),

      // other
      route('Administration', null, 'administration'),
      route('Error', null, '*'),
    ]),
  ],
})

router.beforeEach((to, from, next) => {
  const isAuthenticated = store.getters['auth/isAuth']
  const publicRoutes = store.getters['router/publicRoutes']
  const defaultPublicRoute = store.getters['router/defaultPublicRoute']
  const defaultPrivateRoute = store.getters['router/defaultPrivateRoute']
  const isGoingToAPublicRoute = isGoingToAnAllowedRoute(to, publicRoutes)
  if (isAuthenticated) {
    if (isGoingToAPublicRoute) {
      next({ name: defaultPrivateRoute })
    } else {
      next()
    }
  } else {
    if (getCurrentUserFromLocalStorage()) {
      store.dispatch('app/initializeOnReload')
        .then(() => {
          if (!isGoingToAPublicRoute) {
            next()
          } else {
            next({ name: defaultPrivateRoute })
          }
        })
    } else {
      if (isGoingToAPublicRoute) {
        next()
      } else {
        next({ name: defaultPublicRoute })
      }
    }
  }
})

router.beforeEach((to, from, next) => {
  return to.path.endsWith('/') ? next() : next(trailingSlash(to.path))
})

export default router
