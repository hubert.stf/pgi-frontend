import Vue from 'vue'

export const UNAUTHORIZED = 401
export const PAGE_EXPIRED = 419
export const UNPROCESSABLE_ENTITY = 422

const STATUS_CODE = {
  UNAUTHORIZED,
  PAGE_EXPIRED,
  UNPROCESSABLE_ENTITY,
}

Vue.prototype.$STATUS_CODE = STATUS_CODE
