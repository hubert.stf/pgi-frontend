import { axios } from '@/plugins'

export const getPaymentStatuses = () => {
  return axios.get('api/payment-statuses')
}

export const createPaymentStatus = (title, color) => {
  return axios.post('api/payment-statuses', { title, color })
}

export const updatePaymentStatusTitle = (ID, title) => {
  return axios.put('api/payment-statuses/' + ID, {
    title,
  })
}

export const updatePaymentStatusColor = (ID, color) => {
  return axios.put('api/payment-statuses/' + ID, {
    color,
  })
}

export const deletePaymentStatus = ID => {
  return axios.delete('api/payment-statuses/' + ID)
}
