import {
  createPaymentStatus,
  deletePaymentStatus,
  getPaymentStatuses,
  updatePaymentStatusTitle,
  updatePaymentStatusColor,
} from './functions'
import { mapperPaymentStatusFromApiToState } from './mappers'
import { make } from 'vuex-pathify'

const getInitialState = () => {
  return {
    isLoading: false,
    paymentStatuses: [],
  }
}

const state = getInitialState()

const mutations = {
  ...make.mutations(state),
  RESET_STATE: state => {
    Object.assign(state, getInitialState())
  },
}

const actions = {
  getPaymentStatuses: ({ commit }) => {
    commit('isLoading', true)
    return getPaymentStatuses()
      .then(response => {
        const mappedPaymentStatus = response.data.map(mapperPaymentStatusFromApiToState)
        commit('paymentStatuses', mappedPaymentStatus)
      })
      .finally(() => {
        commit('isLoading', false)
      })
  },
  createPaymentStatus: ({ dispatch }, { title, color }) => {
    return createPaymentStatus(title, color)
      .then(() => {
        dispatch('getPaymentStatuses')
      })
  },
  updatePaymentStatusTitle: ({ commit, dispatch }, { ID, title }) => {
    commit('isLoading', true)
    return updatePaymentStatusTitle(ID, title)
      .then(() => {
        dispatch('getPaymentStatuses')
      })
      .catch(error => {
        commit('isLoading', false)
        throw error
      })
  },
  updatePaymentStatusColor: ({ commit, dispatch }, { ID, color }) => {
    commit('isLoading', true)
    return updatePaymentStatusColor(ID, color)
      .then(() => {
        dispatch('getPaymentStatuses')
      })
      .catch(error => {
        commit('isLoading', false)
        throw error
      })
  },
  deletePaymentStatus: ({ dispatch }, { ID }) => {
    return deletePaymentStatus(ID)
      .then(() => {
        dispatch('getPaymentStatuses')
      })
  },
  resetState: ({ commit }) => {
    commit('RESET_STATE')
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
