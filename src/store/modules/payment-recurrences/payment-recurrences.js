import {
  createPaymentRecurrence,
  deletePaymentRecurrence,
  getPaymentRecurrences,
  updatePaymentRecurrence,
} from './functions'
import { mapperPaymentRecurrenceFromApiToState } from './mappers'
import { make } from 'vuex-pathify'

const getInitialState = () => {
  return {
    isLoading: false,
    paymentRecurrences: [],
  }
}

const state = getInitialState()

const mutations = {
  ...make.mutations(state),
  RESET_STATE: state => {
    Object.assign(state, getInitialState())
  },
}

const actions = {
  getPaymentRecurrences: ({ commit }) => {
    commit('isLoading', true)
    return getPaymentRecurrences()
      .then(response => {
        const mappedPaymentRecurrences = response.data.map(mapperPaymentRecurrenceFromApiToState)
        commit('paymentRecurrences', mappedPaymentRecurrences)
      })
      .finally(() => {
        commit('isLoading', false)
      })
  },
  createPaymentRecurrence: ({ dispatch }, { title }) => {
    return createPaymentRecurrence(title)
      .then(() => {
        dispatch('getPaymentRecurrences')
      })
  },
  updatePaymentRecurrence: ({ commit, dispatch }, { ID, title }) => {
    commit('isLoading', true)
    return updatePaymentRecurrence(ID, title)
      .then(() => {
        dispatch('getPaymentRecurrences')
      })
      .catch(error => {
        commit('isLoading', false)
        throw error
      })
  },
  deletePaymentRecurrence: ({ dispatch }, { ID }) => {
    return deletePaymentRecurrence(ID)
      .then(() => {
        dispatch('getPaymentRecurrences')
      })
  },
  resetState: ({ commit }) => {
    commit('RESET_STATE')
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
