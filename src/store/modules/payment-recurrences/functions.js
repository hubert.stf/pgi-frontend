import { axios } from '@/plugins'

export const getPaymentRecurrences = () => {
  return axios.get('api/payment-recurrences')
}

export const createPaymentRecurrence = title => {
  return axios.post('api/payment-recurrences', { title })
}

export const updatePaymentRecurrence = (ID, title) => {
  return axios.put('api/payment-recurrences/' + ID, {
    title,
  })
}

export const deletePaymentRecurrence = ID => {
  return axios.delete('api/payment-recurrences/' + ID)
}
