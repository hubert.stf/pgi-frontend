import { axios } from '@/plugins'

export const getPaymentMethods = () => {
  return axios.get('api/payment-methods')
}

export const createPaymentMethod = (title, color) => {
  return axios.post('api/payment-methods', { title, color })
}

export const updatePaymentMethodTitle = (ID, title) => {
  return axios.put('api/payment-methods/' + ID, {
    title,
  })
}

export const updatePaymentMethodColor = (ID, color) => {
  return axios.put('api/payment-methods/' + ID, {
    color,
  })
}

export const deletePaymentMethod = ID => {
  return axios.delete('api/payment-methods/' + ID)
}
