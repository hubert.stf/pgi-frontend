export const mapperPaymentMethodFromApiToState = paymentMethod => {
  const {
    id,
    // eslint-disable-next-line camelcase
    user_id,
    title,
    color,
    // eslint-disable-next-line camelcase
    created_at,
    // eslint-disable-next-line camelcase
    updated_at,
    // eslint-disable-next-line camelcase
    expenses_count,
  } = paymentMethod
  return {
    id,
    userId: user_id,
    title,
    color,
    count: expenses_count,
    createdAt: created_at,
    updatedAt: updated_at,
  }
}
