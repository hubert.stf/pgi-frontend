import {
  createPaymentMethod,
  deletePaymentMethod,
  getPaymentMethods,
  updatePaymentMethodTitle,
  updatePaymentMethodColor,
} from './functions'
import { mapperPaymentMethodFromApiToState } from './mappers'
import { make } from 'vuex-pathify'

const getInitialState = () => {
  return {
    isLoading: false,
    paymentMethods: [],
  }
}

const state = getInitialState()

const mutations = {
  ...make.mutations(state),
  RESET_STATE: state => {
    Object.assign(state, getInitialState())
  },
}

const actions = {
  getPaymentMethods: ({ commit }) => {
    commit('isLoading', true)
    return getPaymentMethods()
      .then(response => {
        const mappedPaymentMethod = response.data.map(mapperPaymentMethodFromApiToState)
        commit('paymentMethods', mappedPaymentMethod)
      })
      .finally(() => {
        commit('isLoading', false)
      })
  },
  createPaymentMethod: ({ dispatch }, { title, color }) => {
    return createPaymentMethod(title, color)
      .then(() => {
        dispatch('getPaymentMethods')
      })
  },
  updatePaymentMethodTitle: ({ commit, dispatch }, { ID, title }) => {
    commit('isLoading', true)
    return updatePaymentMethodTitle(ID, title)
      .then(() => {
        dispatch('getPaymentMethods')
      })
      .catch(error => {
        commit('isLoading', false)
        throw error
      })
  },
  updatePaymentMethodColor: ({ commit, dispatch }, { ID, color }) => {
    commit('isLoading', true)
    return updatePaymentMethodColor(ID, color)
      .then(() => {
        dispatch('getPaymentMethods')
      })
      .catch(error => {
        commit('isLoading', false)
        throw error
      })
  },
  deletePaymentMethod: ({ dispatch }, { ID }) => {
    return deletePaymentMethod(ID)
      .then(() => {
        dispatch('getPaymentMethods')
      })
  },
  resetState: ({ commit }) => {
    commit('RESET_STATE')
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
