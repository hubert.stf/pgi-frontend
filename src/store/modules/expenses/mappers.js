/* eslint-disable camelcase */

export const mapperExpenseAmountFromApiToState = expenseAmount => {
  const {
    id,
    expense_id,
    reference_month,
    due_date,
    amount,
    paid_out_at,
    is_payment_confirmed,
    created_at,
    updated_at,
    deleted_at,
  } = expenseAmount
  return {
    id,
    expenseId: expense_id,
    referenceMonth: reference_month,
    dueDate: due_date,
    amount,
    paidOutAt: paid_out_at,
    isPaymentConfirmed: is_payment_confirmed,
    createdAt: created_at,
    updatedAt: updated_at,
    deletedAt: deleted_at,
  }
}

const getTitleFromObject = object => {
  return object && object.title
}

export const mapperExpenseFromApiToState = expense => {
  const {
    id,
    period_id,
    category,
    category_id,
    payment_recurrence,
    payment_recurrence_id,
    payment_status,
    payment_status_id,
    payment_method,
    payment_method_id,
    company_nickname,
    description,
    purchase_date,
    number_of_installments,
    expense_amounts_sum_amount,
    expense_amounts,
    created_at,
    updated_at,
    deleted_at,
  } = expense
  return {
    id,
    periodId: period_id,
    category: getTitleFromObject(category),
    categoryId: category_id,
    paymentRecurrence: getTitleFromObject(payment_recurrence),
    paymentRecurrenceId: payment_recurrence_id,
    paymentStatus: getTitleFromObject(payment_status),
    paymentStatusId: payment_status_id,
    paymentMethod: getTitleFromObject(payment_method),
    paymentMethodId: payment_method_id,
    companyNickname: company_nickname,
    description,
    purchaseDate: purchase_date,
    numberOfInstallments: number_of_installments,
    amountsSum: expense_amounts_sum_amount,
    installments: expense_amounts.map(mapperExpenseAmountFromApiToState),
    createdAt: created_at,
    updatedAt: updated_at,
    deletedAt: deleted_at,
  }
}
