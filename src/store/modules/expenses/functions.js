import { axios } from '@/plugins'

export const getExpenses = periodId => {
  return axios.get('api/expenses', {
    params: {
      period_id: periodId,
    },
  })
}

export const createExpense = form => {
  const {
    categoryId,
    companyNickname,
    description,
    installments,
    numberOfInstallments,
    paymentMethodId,
    paymentRecurrenceId,
    paymentStatusId,
    periodId,
    purchaseDate,
  } = form
  return axios.post('api/expenses', {
    category_id: categoryId,
    company_nickname: companyNickname,
    description,
    expense_amounts: installments.map(installment => {
      const { amount, dueDate, paidOutAt, referenceMonth } = installment
      return {
        amount,
        due_date: dueDate,
        paid_out_at: paidOutAt,
        reference_month: referenceMonth,
      }
    }),
    number_of_installments: numberOfInstallments,
    payment_method_id: paymentMethodId,
    payment_recurrence_id: paymentRecurrenceId,
    payment_status_id: paymentStatusId,
    period_id: periodId,
    purchase_date: purchaseDate,
  })
}

export const updateExpense = form => {
  const {
    id,
    purchaseDate,
    categoryId,
    companyNickname,
    description,
    paymentMethodId,
    paymentStatusId,
    paymentRecurrenceId,
    numberOfInstallments,
    installments,
  } = form
  return axios.put('api/expenses/' + id, {
    id,
    purchase_date: purchaseDate,
    category_id: categoryId,
    company_nickname: companyNickname,
    description,
    payment_method_id: paymentMethodId,
    payment_status_id: paymentStatusId,
    payment_recurrence_id: paymentRecurrenceId,
    number_of_installments: numberOfInstallments,
    expense_amounts: installments.map(installment => {
      const { referenceMonth, amount, dueDate, paidOutAt } = installment
      const mappedInstallment = {
        reference_month: referenceMonth,
        amount,
        due_date: dueDate,
        paid_out_at: paidOutAt,
      }
      return installment.id
        ? { id: installment.id, ...mappedInstallment }
        : mappedInstallment
    }),
  })
}

export const deleteExpense = ID => {
  return axios.delete('api/expenses/' + ID)
}
