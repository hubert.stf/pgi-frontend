import {
  getExpenses,
  createExpense,
  deleteExpense,
  updateExpense,
} from './functions'
import { mapperExpenseFromApiToState } from './mappers'
import { make } from 'vuex-pathify'
import { keyBy, values } from 'lodash'

const getInitialState = () => {
  return {
    isLoading: false,
    expenses: {},
  }
}

const state = getInitialState()

const mutations = {
  ...make.mutations(state),
  UPDATE_EXPENSE: (state, updatedExpense) => {
    state.expenses[updatedExpense.id] = updatedExpense
  },
  RESET_STATE: state => {
    Object.assign(state, getInitialState())
  },
}

const actions = {
  getExpenses: ({ commit, rootGetters }) => {
    const selectedPeriod = rootGetters['periods/getSelectedPeriod']
    if (!selectedPeriod || !selectedPeriod.id) {
      return
    }

    commit('isLoading', true)
    return getExpenses(selectedPeriod.id)
      .then(response => {
        const mappedExpenses = response.data.map(mapperExpenseFromApiToState)
        commit('expenses', keyBy(mappedExpenses, 'id'))
      })
      .finally(() => {
        commit('isLoading', false)
      })
  },
  createExpense: ({ dispatch }, { form }) => {
    return createExpense(form)
      .then(() => {
        dispatch('getExpenses')
      })
  },
  deleteExpense: ({ dispatch }, { ID }) => {
    return deleteExpense(ID)
      .then(() => {
        dispatch('getExpenses')
      })
  },
  updateExpense: ({ commit }, { form }) => {
    return updateExpense(form)
      .then(response => {
        const mappedExpense = mapperExpenseFromApiToState(response.data)
        commit('UPDATE_EXPENSE', mappedExpense)
      })
  },
  resetState: ({ commit }) => {
    commit('RESET_STATE')
  },
}

const getters = {
  all: state => {
    return values(state.expenses)
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
