import {
  createIncomeAmount,
  getIncomeAmountsByIncomeId,
  deleteIncomeAmount,
  updateIncomeAmountDueDate,
  updateIncomeAmountIsPaymentConfirmed,
  updateIncomeAmountItself,
  updateIncomeAmountPaymentReceivedAt,
  updateIncomeAmountReferenceMonth,
} from './functions'
import { mapperIncomeAmountFromApiToState } from './mappers'
import { make } from 'vuex-pathify'

const getInitialState = () => {
  return {
    isLoading: false,
  }
}

const state = getInitialState()

const mutations = {
  ...make.mutations(state),
  RESET_STATE: state => {
    Object.assign(state, getInitialState())
  },
}

const actions = {
  getIncomeAmountsByIncomeId: ({ commit, dispatch }, { incomeId }) => {
    commit('isLoading', true)
    return getIncomeAmountsByIncomeId(incomeId)
      .then(response => {
        const mappedIncomeAmounts = response.data.map(mapperIncomeAmountFromApiToState)
        dispatch('incomes/addAmountsToIncome', {
          incomeId,
          amounts: mappedIncomeAmounts,
        }, { root: true })
      })
      .finally(() => {
        commit('isLoading', false)
      })
  },
  getIncomeAndYoursIncomeAmounts: ({ dispatch }, { incomeId }) => {
    dispatch('incomes/getIncome', { incomeId }, { root: true })
      .then(() => {
        dispatch('getIncomeAmountsByIncomeId', { incomeId })
      })
  },
  createIncomeAmount: ({ dispatch }, { incomeId, referenceMonth, amount, dueDate }) => {
    return createIncomeAmount(incomeId, referenceMonth, amount, dueDate)
      .then(() => {
        dispatch('getIncomeAndYoursIncomeAmounts', { incomeId })
      })
  },
  updateIncomeAmountDueDate: ({ dispatch }, { ID, dueDate }) => {
    return updateIncomeAmountDueDate(ID, dueDate)
      .then(response => {
        const { income_id: incomeId } = response.data
        dispatch('getIncomeAndYoursIncomeAmounts', { incomeId })
      })
  },
  updateIncomeAmountIsPaymentConfirmed: ({ dispatch }, { ID, isPaymentConfirmed }) => {
    return updateIncomeAmountIsPaymentConfirmed(ID, isPaymentConfirmed)
      .then(response => {
        const { income_id: incomeId } = response.data
        dispatch('getIncomeAndYoursIncomeAmounts', { incomeId })
      })
  },
  updateIncomeAmountItself: ({ dispatch }, { ID, amount }) => {
    return updateIncomeAmountItself(ID, amount)
      .then(response => {
        const { income_id: incomeId } = response.data
        dispatch('getIncomeAndYoursIncomeAmounts', { incomeId })
      })
  },
  updateIncomeAmountPaymentReceivedAt: ({ dispatch }, { ID, paymentReceivedAt }) => {
    return updateIncomeAmountPaymentReceivedAt(ID, paymentReceivedAt)
      .then(response => {
        const { income_id: incomeId } = response.data
        dispatch('getIncomeAndYoursIncomeAmounts', { incomeId })
      })
  },
  updateIncomeAmountReferenceMonth: ({ dispatch }, { ID, referenceMonth }) => {
    return updateIncomeAmountReferenceMonth(ID, referenceMonth)
      .then(response => {
        const { income_id: incomeId } = response.data
        dispatch('getIncomeAndYoursIncomeAmounts', { incomeId })
      })
  },
  deleteIncomeAmount: ({ dispatch }, { ID }) => {
    return deleteIncomeAmount(ID)
      .then(response => {
        const { income_id: incomeId } = response.data
        dispatch('getIncomeAndYoursIncomeAmounts', { incomeId })
      })
  },
  resetState: ({ commit }) => {
    commit('RESET_STATE')
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
