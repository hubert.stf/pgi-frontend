export const mapperIncomeAmountFromApiToState = incomeAmount => {
  const {
    id,
    // eslint-disable-next-line camelcase
    income_id,
    // eslint-disable-next-line camelcase
    reference_month,
    // eslint-disable-next-line camelcase
    due_date,
    amount,
    // eslint-disable-next-line camelcase
    payment_received_at,
    // eslint-disable-next-line camelcase
    is_payment_confirmed,
    // eslint-disable-next-line camelcase
    created_at,
    // eslint-disable-next-line camelcase
    updated_at,
    // eslint-disable-next-line camelcase
    deleted_at,
  } = incomeAmount
  return {
    id,
    incomeId: income_id,
    referenceMonth: reference_month,
    dueDate: due_date,
    amount,
    paymentReceivedAt: payment_received_at,
    isPaymentConfirmed: is_payment_confirmed,
    createdAt: created_at,
    updatedAt: updated_at,
    deletedAt: deleted_at,
  }
}
