import { axios } from '@/plugins'

export const getIncomeAmountsByIncomeId = incomeId => {
  return axios.get('api/income-amounts/' + incomeId)
}

export const createIncomeAmount = (incomeId, referenceMonth, amount, dueDate) => {
  return axios.post('api/income-amounts', {
    income_id: incomeId,
    reference_month: referenceMonth,
    amount,
    due_date: dueDate,
  })
}

export const updateIncomeAmountReferenceMonth = (ID, referenceMonth) => {
  return axios.put('api/income-amounts/' + ID, {
    reference_month: referenceMonth,
  })
}

export const updateIncomeAmountDueDate = (ID, dueDate) => {
  return axios.put('api/income-amounts/' + ID, {
    due_date: dueDate,
  })
}

export const updateIncomeAmountItself = (ID, amount) => {
  return axios.put('api/income-amounts/' + ID, {
    amount,
  })
}

export const updateIncomeAmountPaymentReceivedAt = (ID, paymentReceivedAt) => {
  return axios.put('api/income-amounts/' + ID, {
    payment_received_at: paymentReceivedAt,
  })
}

export const updateIncomeAmountIsPaymentConfirmed = (ID, isPaymentConfirmed) => {
  return axios.put('api/income-amounts/' + ID, {
    is_payment_confirmed: isPaymentConfirmed,
  })
}

export const deleteIncomeAmount = ID => {
  return axios.delete('api/income-amounts/' + ID)
}
