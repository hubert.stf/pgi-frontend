import {
  getStats,
  getChartData,
} from './functions'
import { make } from 'vuex-pathify'

const state = {
  isLoading: false,
}

const mutations = make.mutations(state)

const actions = {
  getStats: ({ commit }, periodId) => {
    commit('isLoading', true)
    return getStats(periodId)
      .finally(() => {
        commit('isLoading', false)
      })
  },
  getChartData: ({ commit }, periodId) => {
    commit('isLoading', true)
    return getChartData(periodId)
      .finally(() => {
        commit('isLoading', false)
      })
  },
}

const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
