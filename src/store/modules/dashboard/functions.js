import { axios } from '@/plugins'

export const getStats = periodId => {
  return axios.get('/api/dashboard/stats/' + periodId)
}

export const getChartData = periodId => {
  return axios.get('/api/dashboard/chart-data/' + periodId)
}
