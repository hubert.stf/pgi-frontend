const state = {
  publicRoutes: [
    'LoginPage',
    'RegisterPage',
  ],
  defaultPublicRoute: 'LoginPage',
  defaultPrivateRoute: 'Dashboard',
}

const getters = {
  publicRoutes: state => {
    return state.publicRoutes
  },
  defaultPublicRoute: state => {
    return state.defaultPublicRoute
  },
  defaultPrivateRoute: state => {
    return state.defaultPrivateRoute
  },
}

export default {
  namespaced: true,
  state,
  getters,
}
