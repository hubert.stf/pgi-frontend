import { removeXsrfToken } from '@/services/cookies'
import { axios } from '@/plugins'

export const setCRSFCookie = () => {
  return axios.get('/sanctum/csrf-cookie')
}

export const login = (email, password, remember) => {
  const optionalFields = { ...(remember && { remember: remember }) }
  return axios.post('/api/login', {
    email,
    password,
    ...optionalFields,
  })
}

export const register = (firstName, lastName, email, password, passwordConfirmation) => {
  return axios.post('/api/register', {
    first_name: firstName,
    last_name: lastName,
    email,
    password,
    password_confirmation: passwordConfirmation,
  })
}

export const logout = () => {
  return axios.post('/api/logout')
    .then(() => {
      removeXsrfToken()
    })
}

export const getUser = () => {
  return axios.get('/api/user')
}

export const checksIfTheEmailHasAlreadyBeenTaken = email => {
  return axios.post('/api/user/email/verify', { email })
}

export const updateAuthUserFullName = (firstName, lastName) => {
  return axios.put('api/user/update-fullname', {
    first_name: firstName,
    last_name: lastName,
  })
}

export const updateAuthUserLanguage = language => {
  return axios.put('api/user/update-language', {
    language,
  })
}
