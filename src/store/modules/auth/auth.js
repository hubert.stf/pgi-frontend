import {
  setCRSFCookie,
  login,
  register,
  logout,
  getUser,
  checksIfTheEmailHasAlreadyBeenTaken,
  updateAuthUserFullName,
  updateAuthUserLanguage,
} from './functions'
import { mapperUserFromApiToState } from './mappers'
import {
  setCurrentUser as setCurrentUserInLocalStorage,
  removeCurrentUser as removeCurrentUserFromLocalStorage,
} from '@/services/local-storage'
import { setI18Nlocale, setMomentLocale } from '@/plugins'
import { make } from 'vuex-pathify'

const authUserInitialState = {
  id: null,
  firstName: null,
  lastName: null,
  email: null,
  emailVerifiedAt: null,
  language: null,
  createdAt: null,
  updatedAt: null,
  deletedAt: null,
}

const state = {
  isAuth: false,
  authUser: authUserInitialState,
}

const mutations = {
  ...make.mutations(state),
  SET_AUTH_STATUS (state, status) {
    state.isAuth = status
  },
  SET_AUTH_USER (state, user) {
    state.authUser = user
  },
}

const actions = {
  setAuthUser: ({ commit }) => {
    return getUser()
      .then(user => {
        const mappedUser = mapperUserFromApiToState(user)
        setCurrentUserInLocalStorage(mappedUser)
        commit('SET_AUTH_USER', mappedUser)
        commit('SET_AUTH_STATUS', true)
        return mappedUser
      })
  },
  login: ({ commit, dispatch }, { email, password, remember }) => {
    return setCRSFCookie()
      .then(() => {
        return login(email, password, remember)
          .then(async () => {
            await dispatch('app/initialize', {}, { root: true })
          })
      })
  },
  register: ({ commit }, { firstName, lastName, email, password, passwordConfirmation }) => {
    return setCRSFCookie()
      .then(() => {
        return register(firstName, lastName, email, password, passwordConfirmation)
      })
  },
  checksIfTheEmailHasAlreadyBeenTaken: (store, email) => {
    return setCRSFCookie()
      .then(() => {
        return checksIfTheEmailHasAlreadyBeenTaken(email)
      })
  },
  cleanAuthUser: ({ commit }) => {
    removeCurrentUserFromLocalStorage()
    commit('SET_AUTH_USER', authUserInitialState)
    commit('SET_AUTH_STATUS', false)
  },
  logout: ({ dispatch }) => {
    return logout()
      .then(() => {
        dispatch('cleanAuthUser')
        dispatch('app/destroy', {}, { root: true })
      })
  },
  updateAuthUserFullName: ({ dispatch }, { firstName, lastName }) => {
    return updateAuthUserFullName(firstName, lastName)
      .then(() => {
        return dispatch('setAuthUser')
      })
  },
  updateAuthUserLanguage: ({ dispatch }, { language }) => {
    return updateAuthUserLanguage(language)
      .then(() => {
        setI18Nlocale(language)
        setMomentLocale(language)
        return dispatch('setAuthUser')
      })
  },
}

const getters = {
  isAuth: state => {
    return state.isAuth
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
