export const mapperUserFromApiToState = user => {
  const {
    id,
    // eslint-disable-next-line camelcase
    first_name,
    // eslint-disable-next-line camelcase
    last_name,
    email,
    // eslint-disable-next-line camelcase
    email_verified_at,
    language,
    // eslint-disable-next-line camelcase
    created_at,
    // eslint-disable-next-line camelcase
    updated_at,
    // eslint-disable-next-line camelcase
    deleted_at,
  } = user.data
  return {
    id,
    firstName: first_name,
    lastName: last_name,
    email,
    emailVerifiedAt: email_verified_at,
    language,
    createdAt: created_at,
    updatedAt: updated_at,
    deletedAt: deleted_at,
  }
}
