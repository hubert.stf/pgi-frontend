import {
  createIncome,
  deleteIncome,
  getIncomes,
  updateIncome,
  getIncome,
} from './functions'
import { mapperIncomeFromApiToState } from './mappers'
import { make } from 'vuex-pathify'
import { keyBy, values } from 'lodash'

const getInitialState = () => {
  return {
    isLoading: false,
    incomes: {},
  }
}

const state = getInitialState()

const mutations = {
  ...make.mutations(state),
  ADD_AMOUNTS_TO_INCOME: (state, { incomeId, amounts }) => {
    state.incomes[incomeId].amounts = amounts
  },
  UPDATE_INCOME: (state, income) => {
    state.incomes[income.id] = income
  },
  RESET_STATE: state => {
    Object.assign(state, getInitialState())
  },
}

const actions = {
  getIncomes: ({ commit, rootGetters }) => {
    const selectedPeriod = rootGetters['periods/getSelectedPeriod']
    if (!selectedPeriod || !selectedPeriod.id) {
      return
    }

    commit('isLoading', true)
    return getIncomes(selectedPeriod.id)
      .then(response => {
        const mappedIncomes = response.data.map(mapperIncomeFromApiToState)
        commit('incomes', keyBy(mappedIncomes, 'id'))
      })
      .finally(() => {
        commit('isLoading', false)
      })
  },
  getIncome: ({ commit }, { incomeId }) => {
    commit('isLoading', true)
    return getIncome(incomeId)
      .then(response => {
        const mappedIncome = mapperIncomeFromApiToState(response.data)
        commit('UPDATE_INCOME', mappedIncome)
      })
      .finally(() => {
        commit('isLoading', false)
      })
  },
  createIncome: ({ dispatch }, { title, periodId }) => {
    return createIncome(title, periodId)
      .then(() => {
        dispatch('getIncomes')
      })
  },
  updateIncome: ({ commit, dispatch }, { ID, title }) => {
    commit('isLoading', true)
    return updateIncome(ID, title)
      .then(() => {
        dispatch('getIncomes')
      })
      .catch(error => {
        commit('isLoading', false)
        throw error
      })
  },
  deleteIncome: ({ dispatch }, { ID }) => {
    return deleteIncome(ID)
      .then(() => {
        dispatch('getIncomes')
      })
  },
  addAmountsToIncome: ({ commit }, { incomeId, amounts }) => {
    commit('ADD_AMOUNTS_TO_INCOME', { incomeId, amounts })
  },
  resetState: ({ commit }) => {
    commit('RESET_STATE')
  },
}

const getters = {
  all: state => {
    return values(state.incomes)
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
