export const mapperIncomeFromApiToState = income => {
  const {
    id,
    // eslint-disable-next-line camelcase
    period_id,
    title,
    // eslint-disable-next-line camelcase
    income_amounts_sum_amount,
    // eslint-disable-next-line camelcase
    created_at,
    // eslint-disable-next-line camelcase
    updated_at,
    // eslint-disable-next-line camelcase
    deleted_at,
  } = income
  return {
    id,
    periodId: period_id,
    title,
    amounts: [],
    amountsSum: income_amounts_sum_amount,
    createdAt: created_at,
    updatedAt: updated_at,
    deletedAt: deleted_at,
  }
}
