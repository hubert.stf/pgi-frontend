import { axios } from '@/plugins'

export const getIncomes = periodId => {
  return axios.get('api/incomes', {
    params: {
      period_id: periodId,
    },
  })
}

export const getIncome = incomeId => {
  return axios.get('api/incomes/' + incomeId)
}

export const createIncome = (title, periodId) => {
  return axios.post('api/incomes', {
    title,
    period_id: periodId,
  })
}

export const updateIncome = (ID, title) => {
  return axios.put('api/incomes/' + ID, {
    title,
  })
}

export const deleteIncome = ID => {
  return axios.delete('api/incomes/' + ID)
}
