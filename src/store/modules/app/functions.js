import { keyBy } from 'lodash'
import { i18n } from '@/plugins'

export const flatItems = (items) => {
  return items.reduce((previousValue, item) => {
    if (item.items) {
      return [...previousValue, item, ...item.items]
    }
    return [...previousValue, item]
  }, [])
}

export const arrayToObject = (array, key) => {
  return keyBy(array, key)
}

export const translateItemTitle = (key, period) => {
  if (period) {
    return i18n.t('global.routes.' + key + 'WithPeriod', { period })
  }
  return i18n.t('global.routes.' + key)
}
