import { make } from 'vuex-pathify'
import {
  flatItems,
  arrayToObject,
  translateItemTitle,
} from './functions'
import { i18n } from '@/plugins'

const state = {
  drawer: true,
  mini: false,
  items: [
    {
      title: 'Dashboard',
      icon: 'mdi-view-dashboard',
      to: '/',
      period: true,
    },
    {
      title: 'Incomes',
      icon: 'mdi-currency-usd',
      to: '/incomes',
      period: true,
    },
    {
      title: 'Expenses',
      icon: 'mdi-currency-usd-off',
      to: '/expenses',
      period: true,
    },
    {
      title: 'Settings',
      icon: 'mdi-cog',
      items: [
        {
          title: 'Periods',
          icon: 'mdi-calendar',
          to: '/settings/periods',
        },
        {
          title: 'Categories',
          icon: 'mdi-shape-outline',
          to: '/settings/categories',
        },
        {
          title: 'PaymentMethods',
          icon: 'mdi-credit-card-outline',
          to: '/settings/payment-methods',
        },
        {
          title: 'PaymentRecurrences',
          icon: 'mdi-clock-outline',
          to: '/settings/payment-recurrences',
        },
        {
          title: 'PaymentStatuses',
          icon: 'mdi-list-status',
          to: '/settings/payment-statuses',
        },
      ],
    },
  ],
  flattedItems: {},
  isLoading: false,
}

const mutations = make.mutations(state)

const actions = {
  initialize: async ({ dispatch }) => {
    dispatch('flatItems')
    await dispatch('auth/setAuthUser', {}, { root: true })
    await dispatch('periods/initialize', {}, { root: true })
    await dispatch('periods/getPeriods', {}, { root: true })
    await dispatch('categories/getCategories', {}, { root: true })
    await dispatch('paymentMethods/getPaymentMethods', {}, { root: true })
    await dispatch('paymentRecurrences/getPaymentRecurrences', {}, { root: true })
    await dispatch('paymentStatuses/getPaymentStatuses', {}, { root: true })
  },
  initializeOnReload: async ({ commit, dispatch }) => {
    commit('isLoading', true)
    dispatch('initialize')
      .finally(() => {
        commit('isLoading', false)
      })
  },
  flatItems: ({ state, commit }) => {
    const flattedItems = flatItems(state.items)
    const itemsObject = arrayToObject(flattedItems, 'title')
    commit('flattedItems', itemsObject)
  },
  destroy: ({ dispatch }) => {
    dispatch('categories/resetState', {}, { root: true })
    dispatch('paymentMethods/resetState', {}, { root: true })
    dispatch('paymentRecurrences/resetState', {}, { root: true })
    dispatch('paymentStatuses/resetState', {}, { root: true })
    dispatch('periods/resetState', {}, { root: true })
    dispatch('incomes/resetState', {}, { root: true })
    dispatch('incomeAmounts/resetState', {}, { root: true })
    dispatch('expenses/resetState', {}, { root: true })
  },
}

const getters = {
  selectedPeriodTitle: (state, getters, rootState, rootGetters) => {
    const selectedPeriod = rootGetters['periods/getSelectedPeriod']
    return selectedPeriod && selectedPeriod.title
  },
  currentRouteName: (state, getters, rootState) => {
    const flattedItems = state.flattedItems
    const currentRouteName = rootState.route.name
    const selectedPeriodTitle = getters.selectedPeriodTitle || i18n.t('global.messages.noPeriodSelected')

    return flattedItems[currentRouteName] && flattedItems[currentRouteName].period
      ? translateItemTitle(currentRouteName, selectedPeriodTitle)
      : translateItemTitle(currentRouteName)
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
