import { axios } from '@/plugins'

export const getPeriods = () => {
  return axios.get('api/periods')
}

export const createPeriod = (title, startDate, endDate) => {
  return axios.post('api/periods', {
    title,
    start_date: startDate,
    end_date: endDate,
  })
}

export const updatePeriodTitle = (ID, title) => {
  return axios.put('api/periods/' + ID, {
    title,
  })
}

export const updatePeriodStartDate = (ID, startDate, endDate) => {
  return axios.put('api/periods/' + ID, {
    start_date: startDate,
    end_date: endDate,
  })
}

export const updatePeriodEndDate = (ID, startDate, endDate) => {
  return axios.put('api/periods/' + ID, {
    start_date: startDate,
    end_date: endDate,
  })
}

export const updatePeriodIsDefault = (ID, isDefault) => {
  return axios.put('api/periods/' + ID, {
    is_default: isDefault,
  })
}

export const updatePeriodIsToDisplay = (ID, isToDisplay) => {
  return axios.put('api/periods/' + ID, {
    is_to_display: isToDisplay,
  })
}

export const deletePeriod = ID => {
  return axios.delete('api/periods/' + ID)
}

export const getDefaultPeriod = periods => {
  return periods.find(period => period.isDefault)
}

export const findPeriodById = (periods, periodId) => {
  return periods.find(period => periodId === period.id)
}
