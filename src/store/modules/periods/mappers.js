export const mapperPeriodFromApiToState = period => {
  const {
    id,
    // eslint-disable-next-line camelcase
    user_id,
    title,
    // eslint-disable-next-line camelcase
    start_date,
    // eslint-disable-next-line camelcase
    end_date,
    // eslint-disable-next-line camelcase
    is_default,
    // eslint-disable-next-line camelcase
    is_to_display,
    // eslint-disable-next-line camelcase
    created_at,
    // eslint-disable-next-line camelcase
    updated_at,
    // eslint-disable-next-line camelcase
    deleted_at,
  } = period
  return {
    id,
    userId: user_id,
    title,
    startDate: start_date,
    endDate: end_date,
    isDefault: is_default,
    isToDisplay: is_to_display,
    createdAt: created_at,
    updatedAt: updated_at,
    deletedAt: deleted_at,
  }
}
