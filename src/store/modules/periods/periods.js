import {
  getPeriods,
  createPeriod,
  updatePeriodIsToDisplay,
  updatePeriodStartDate,
  updatePeriodTitle,
  updatePeriodIsDefault,
  updatePeriodEndDate,
  deletePeriod,
  getDefaultPeriod,
  findPeriodById,
} from './functions'
import { mapperPeriodFromApiToState } from './mappers'
import {
  setSelectedPeriod,
  getSelectedPeriod,
  isSetSelectedPeriod,
  removeSelectedPeriod,
} from '@/services/local-storage'
import { make } from 'vuex-pathify'

const getInitialState = () => {
  return {
    isLoading: false,
    selectedPeriod: {
      id: null,
    },
    periods: [],
  }
}

const state = getInitialState()

const mutations = {
  ...make.mutations(state),
  SET_SELECTED_PERIOD: (state, period) => {
    if (!period || !period.id) {
      return
    }
    state.selectedPeriod.id = period.id
  },
  RESET_SELECTED_PERIOD: state => {
    state.selectedPeriod.id = null
  },
  RESET_STATE: state => {
    Object.assign(state, getInitialState())
  },
}

const actions = {
  initialize: ({ dispatch }) => {
    if (isSetSelectedPeriod()) {
      const defaultPeriodId = getSelectedPeriod()
      dispatch('setManuallySelectedPeriod', { period: { ...defaultPeriodId } })
    }
  },
  getPeriods: ({ commit }) => {
    commit('isLoading', true)
    return getPeriods()
      .then(response => {
        const mappedPeriods = response.data.map(mapperPeriodFromApiToState)
        commit('periods', mappedPeriods)
      })
      .finally(() => {
        commit('isLoading', false)
      })
  },
  createPeriod: ({ dispatch }, { title, startDate, endDate }) => {
    return createPeriod(title, startDate, endDate)
      .then(() => {
        dispatch('getPeriods')
      })
  },
  updatePeriodTitle: ({ commit, dispatch }, { ID, title }) => {
    commit('isLoading', true)
    return updatePeriodTitle(ID, title)
      .then(() => {
        dispatch('getPeriods')
      })
      .catch(error => {
        commit('isLoading', false)
        throw error
      })
  },
  updatePeriodStartDate: ({ commit, dispatch }, { ID, startDate, endDate }) => {
    commit('isLoading', true)
    return updatePeriodStartDate(ID, startDate, endDate)
      .then(() => {
        dispatch('getPeriods')
      })
      .catch(error => {
        commit('isLoading', false)
        throw error
      })
  },
  updatePeriodEndDate: ({ commit, dispatch }, { ID, startDate, endDate }) => {
    commit('isLoading', true)
    return updatePeriodEndDate(ID, startDate, endDate)
      .then(() => {
        dispatch('getPeriods')
      })
      .catch(error => {
        commit('isLoading', false)
        throw error
      })
  },
  updatePeriodIsDefault: ({ commit, dispatch }, { ID, isDefault }) => {
    commit('isLoading', true)
    return updatePeriodIsDefault(ID, isDefault)
      .then(() => {
        dispatch('getPeriods')
      })
      .catch(error => {
        commit('isLoading', false)
        throw error
      })
  },
  updatePeriodIsToDisplay: ({ commit, dispatch }, { ID, isToDisplay }) => {
    commit('isLoading', true)
    return updatePeriodIsToDisplay(ID, isToDisplay)
      .then(() => {
        dispatch('getPeriods')
      })
      .catch(error => {
        commit('isLoading', false)
        throw error
      })
  },
  deletePeriod: ({ commit, dispatch, state }, { ID }) => {
    return deletePeriod(ID)
      .then(() => {
        if (ID === state.selectedPeriod.id) {
          commit('RESET_SELECTED_PERIOD')
          removeSelectedPeriod()
        }
        dispatch('getPeriods')
      })
  },
  setManuallySelectedPeriod: ({ commit }, { period }) => {
    commit('SET_SELECTED_PERIOD', period)
    setSelectedPeriod({ id: period.id })
  },
  resetState: ({ commit }) => {
    commit('RESET_STATE')
    removeSelectedPeriod()
  },
}

const getters = {
  periodsToDisplay: state => {
    return state.periods.filter(period => period.isToDisplay || period.isDefault)
  },
  isThereAPeriodToDisplay: (state, getters) => {
    return !!getters.periodsToDisplay.length
  },
  getSelectedPeriod: state => {
    const { selectedPeriod, periods } = state
    const defaultPeriod = getDefaultPeriod(periods)
    return selectedPeriod && selectedPeriod.id
      ? findPeriodById(periods, selectedPeriod.id)
      : defaultPeriod && defaultPeriod.id
        ? defaultPeriod
        : null
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
