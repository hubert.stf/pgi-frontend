const state = {
  top: true,
  status: false,
  variant: '',
  timeout: null,
  text: '',
}

const mutations = {
  SET_OPTIONS: (state, options) => {
    const { variant, timeout, text } = options
    state.variant = variant
    state.timeout = timeout
    state.text = text
  },
  SET_STATUS: (state, status) => {
    state.status = status
  },
}

const actions = {
  setOptions: ({ commit }, options) => {
    commit('SET_OPTIONS', options)
    commit('SET_STATUS', true)
  },
  setStatus: ({ commit }, status) => {
    commit('SET_STATUS', status)
  },
}

const getters = {
  options: state => {
    return state
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
