const state = {
  drawer: {
    gradient: 0,
    mini: false,
  },
  gradients: [
    'rgba(228, 226, 226, 1), rgba(255, 255, 255, 0.7)',
  ],
}

const getters = {
  gradient: state => {
    return state.gradients[state.drawer.gradient]
  },
}

export default {
  namespaced: true,
  state,
  getters,
}
