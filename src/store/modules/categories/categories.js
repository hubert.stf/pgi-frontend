import {
  createCategory,
  deleteCategory,
  getCategories,
  updateCategory,
} from './functions'
import { mapperCategoryFromApiToState } from './mappers'
import { make } from 'vuex-pathify'

const getInitialState = () => {
  return {
    isLoading: false,
    categories: [],
  }
}

const state = getInitialState()

const mutations = {
  ...make.mutations(state),
  RESET_STATE: state => {
    Object.assign(state, getInitialState())
  },
}

const actions = {
  getCategories: ({ commit }) => {
    commit('isLoading', true)
    return getCategories()
      .then(response => {
        const mappedCategories = response.data.map(mapperCategoryFromApiToState)
        commit('categories', mappedCategories)
      })
      .finally(() => {
        commit('isLoading', false)
      })
  },
  createCategory: ({ dispatch }, { title }) => {
    return createCategory(title)
      .then(() => {
        dispatch('getCategories')
      })
  },
  updateCategory: ({ commit, dispatch }, { ID, title }) => {
    commit('isLoading', true)
    return updateCategory(ID, title)
      .then(() => {
        dispatch('getCategories')
      })
      .catch(error => {
        commit('isLoading', false)
        throw error
      })
  },
  deleteCategory: ({ dispatch }, { ID }) => {
    return deleteCategory(ID)
      .then(() => {
        dispatch('getCategories')
      })
  },
  resetState: ({ commit }) => {
    commit('RESET_STATE')
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
