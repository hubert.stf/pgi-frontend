export const mapperCategoryFromApiToState = category => {
  const {
    id,
    // eslint-disable-next-line camelcase
    user_id,
    title,
    // eslint-disable-next-line camelcase
    created_at,
    // eslint-disable-next-line camelcase
    updated_at,
    // eslint-disable-next-line camelcase
    expenses_count,
  } = category
  return {
    id,
    userId: user_id,
    title,
    count: expenses_count,
    createdAt: created_at,
    updatedAt: updated_at,
  }
}
