import { axios } from '@/plugins'

export const getCategories = () => {
  return axios.get('api/categories')
}

export const createCategory = title => {
  return axios.post('api/categories', { title })
}

export const updateCategory = (ID, title) => {
  return axios.put('api/categories/' + ID, {
    title,
  })
}

export const deleteCategory = ID => {
  return axios.delete('api/categories/' + ID)
}
