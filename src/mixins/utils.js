import { symbolWhenEmpty } from '@/lib/utils'

export default {
  filters: {
    symbolWhenEmpty (value) {
      return symbolWhenEmpty(value)
    },
  },
}
