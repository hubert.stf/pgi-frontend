import { formatCurrency } from '@/lib/money'

export default {
  filters: {
    formatCurrency (amount) {
      return formatCurrency(amount)
    },
  },
}
