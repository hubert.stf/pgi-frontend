import {
  formatDate,
  formatMonth,
  formatDateToRelativeTime,
} from '@/lib/date'

export default {
  filters: {
    formatDateToRelativeTime (date) {
      return formatDateToRelativeTime(date)
    },
    formatDate (date) {
      return formatDate(date)
    },
    formatMonth (yearAndMonth) {
      return formatMonth(yearAndMonth)
    },
  },
}
