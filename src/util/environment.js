export const isInDevelopment = () => {
  return process.env.NODE_ENV === 'development'
}
