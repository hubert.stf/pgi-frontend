import Vue from 'vue'

const XSRF_TOKEN = 'XSRF-TOKEN'

export const removeXsrfToken = () => {
  Vue.$cookies.remove(XSRF_TOKEN)
}
