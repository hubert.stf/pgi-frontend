import store from '@/store'
import { i18n } from '@/plugins'

export const cleanAuthUser = () => {
  store.dispatch('auth/cleanAuthUser')
  store.dispatch('toast/setOptions', {
    variant: 'warning',
    text: i18n.t('global.messages.logoutWarning'),
  })
}
