import router from '@/router'
import store from '@/store'

export const redirectToLogin = () => {
  const defaultPublicRoute = store.getters['router/defaultPublicRoute']
  if (router.history.current.name !== defaultPublicRoute) {
    router.push({ name: defaultPublicRoute })
  }
}
