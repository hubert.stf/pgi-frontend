import store from '@/store'

export const showTheTitleHasAlreadyBeenTakenToast = (responseErrorMessage, toastMessage) => {
  if (responseErrorMessage === 'The title has already been taken.') {
    store.dispatch('toast/setOptions', {
      variant: 'error',
      text: toastMessage,
    })
  }
}
