const { localStorage } = window
const { parse, stringify } = JSON

const CURRENT_USER = 'current-user'

export const setCurrentUser = user => {
  localStorage.setItem(CURRENT_USER, stringify(user))
}

export const removeCurrentUser = () => {
  localStorage.removeItem(CURRENT_USER)
}

export const getCurrentUser = () => {
  return localStorage.getItem(CURRENT_USER)
}

const USER_PREFERENCES = 'user-preferences'

export const setUserPreferences = preferences => {
  localStorage.setItem(USER_PREFERENCES, stringify(preferences))
}

export const getUserPreferences = () => {
  return parse(localStorage.getItem(USER_PREFERENCES))
}

const SELECTED_PERIOD = 'selected-period'

export const setSelectedPeriod = preferences => {
  localStorage.setItem(SELECTED_PERIOD, stringify(preferences))
}

export const removeSelectedPeriod = () => {
  localStorage.removeItem(SELECTED_PERIOD)
}

export const getSelectedPeriod = () => {
  return parse(localStorage.getItem(SELECTED_PERIOD))
}

export const isSetSelectedPeriod = () => {
  return !!localStorage.getItem(SELECTED_PERIOD)
}

const SELECTED_EXPENSES_TABLE_COLUMNS = 'selected-expenses-table-columns'

export const setSelectedExpensesColumns = selectedExpensesColumns => {
  localStorage.setItem(SELECTED_EXPENSES_TABLE_COLUMNS, stringify(selectedExpensesColumns))
}

export const getSelectedExpensesColumns = () => {
  return parse(localStorage.getItem(SELECTED_EXPENSES_TABLE_COLUMNS))
}

export const isSetSelectedExpensesColumns = () => {
  const array = getSelectedExpensesColumns()
  if (!array || !typeof Array) {
    return false
  }

  return !!array.length
}
