export const mapperItemsToSelectField = (
  items, text = 'title', value = 'id', color = 'color',
) => (
  items.map(item => ({
    text: item[text],
    value: item[value],
    color: item[color],
  }))
)
