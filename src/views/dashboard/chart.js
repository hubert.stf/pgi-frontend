import { getCurrentRelativeTime } from '@/lib/date'

export default (i18n, labels, series, high, low) => ({
  type: 'Bar',
  color: 'primary',
  title: i18n('chart.title'),
  subtitle: i18n('chart.subtitle'),
  time: i18n('chart.updated', { time: getCurrentRelativeTime() }),
  data: {
    labels: labels || [],
    series: series || [],
  },
  options: {
    axisY: {
      offset: 50,
      onlyInteger: true,
    },
    high: high || 1,
    low: low < 0 ? low : 0,
    chartPadding: {
      top: 0,
      right: 5,
      bottom: 0,
      left: 0,
    },
    height: 450,
  },
  responsiveOptions: [
    ['screen and (max-width: 640px)', {
      seriesBarDistance: 5,
      axisX: {
        labelInterpolationFnc: function (value) {
          return value[0]
        },
      },
    }],
  ],
})
