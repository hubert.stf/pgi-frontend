import { formatCurrency } from '@/lib/money'
import { symbolWhenEmpty } from '@/lib/utils'

export default (i18n, incomes, expenses, difference) => ([
  {
    actionIcon: 'mdi-information',
    actionText: i18n('stats.income.actionText'),
    color: 'success',
    icon: 'mdi-currency-usd',
    title: i18n('stats.income.title'),
    value: symbolWhenEmpty(formatCurrency(incomes)),
  },
  {
    actionIcon: 'mdi-information',
    actionText: i18n('stats.expense.actionText'),
    color: 'error',
    icon: 'mdi-currency-usd-off',
    title: i18n('stats.expense.title'),
    value: symbolWhenEmpty(formatCurrency(expenses)),
  },
  {
    actionIcon: 'mdi-information',
    actionText: i18n('stats.difference.actionText'),
    color: 'info',
    icon: 'mdi-equal',
    title: i18n('stats.difference.title'),
    value: symbolWhenEmpty(formatCurrency(difference)),
  },
])
