export default i18n => ([
    {
      text: i18n('global.crudDataTable.header.referenceMonth'),
      value: 'referenceMonth',
      align: 'center',
      sortable: true,
    },
    {
      text: i18n('global.crudDataTable.header.amount'),
      value: 'amount',
      align: 'center',
      sortable: true,
    },
    {
      text: i18n('global.crudDataTable.header.dueDate'),
      value: 'dueDate',
      align: 'center',
      sortable: true,
    },
    {
      text: i18n('global.crudDataTable.header.paidOutAt'),
      value: 'paidOutAt',
      align: 'center',
      sortable: true,
    },
    {
      text: i18n('global.crudDataTable.header.createdAt'),
      value: 'createdAt',
      align: 'center',
      sortable: true,
      width: '10%',
    },
    {
      text: i18n('global.crudDataTable.header.updatedAt'),
      value: 'updatedAt',
      align: 'center',
      sortable: true,
      width: '10%',
    },
  ]
)
