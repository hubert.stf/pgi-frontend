export default [
  'purchaseDate',
  'category',
  'paymentStatus',
  'paymentMethod',
  'companyNickname',
  'amountsSum',
  'actions',
  'data-table-expand',
]
