export default {
  'pt-BR': {
    updateSuccess: 'Período atualizado com sucesso!',
    updateError: 'Ops... Algo deu errado ao tentar atualizar o período. Por favor, tente novamente.',
  },
  'en-US': {
    updateSuccess: 'Period successfully updated!',
    updateError: 'Oops... Something went wrong while trying to update the period. Please try again.',
  },
}
