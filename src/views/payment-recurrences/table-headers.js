export default i18n => ([
  {
    text: i18n('global.crudDataTable.header.title'),
    value: 'title',
    align: 'center',
    sortable: true,
    width: '40%',
    filterable: true,
  },
  {
    text: i18n('global.crudDataTable.header.count'),
    value: 'count',
    align: 'center',
    sortable: true,
    width: '15%',
  },
  {
    text: i18n('global.crudDataTable.header.createdAt'),
    value: 'createdAt',
    align: 'center',
    sortable: true,
    width: '15%',
  },
  {
    text: i18n('global.crudDataTable.header.updatedAt'),
    value: 'updatedAt',
    align: 'center',
    sortable: true,
    width: '15%',
  },
  {
    text: i18n('global.crudDataTable.header.delete'),
    value: 'delete',
    align: 'center',
    sortable: false,
    width: '15%',
  },
])
