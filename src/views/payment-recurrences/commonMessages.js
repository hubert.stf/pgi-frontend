export default {
  'pt-BR': {
    paymentRecurrenceIsAlreadyRegisteredError: 'A recorrência "{title}" já está cadastrada. Por favor, informe outro título.',
  },
  'en-US': {
    paymentRecurrenceIsAlreadyRegisteredError: 'The "{title}" payment recurrence is already registered. Please enter another title.',
  },
}
