export default {
  'pt-BR': {
    categoryIsAlreadyRegisteredError: 'A categoria "{title}" já está cadastrada. Por favor, informe outro título.',
  },
  'en-US': {
    categoryIsAlreadyRegisteredError: 'The "{title}" category is already registered. Please enter another title.',
  },
}
