export default {
  'pt-BR': {
    updateSuccess: 'Status atualizada com sucesso!',
    updateError: 'Ops... Algo deu errado ao tentar atualizar o status. Por favor, tente novamente.',
    paymentStatusIsAlreadyRegisteredError: 'O status "{title}" já está cadastrado. Por favor, informe outro título.',
  },
  'en-US': {
    updateSuccess: 'Payment status successfully updated!',
    updateError: 'Oops... Something went wrong while trying to update the payment status. Please try again.',
    paymentStatusIsAlreadyRegisteredError: 'The "{title}" payment status is already registered. Please enter another title.',
  },
}
