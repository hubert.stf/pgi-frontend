export default {
  'pt-BR': {
    updateSuccess: 'Via de pagamento atualizada com sucesso!',
    updateError: 'Ops... Algo deu errado ao tentar atualizar a via de pagamento. Por favor, tente novamente.',
    paymentMethodIsAlreadyRegisteredError: 'A via de pagamento "{title}" já está cadastrada. Por favor, informe outro título.',
  },
  'en-US': {
    updateSuccess: 'Payment method successfully updated!',
    updateError: 'Oops... Something went wrong while trying to update the payment method. Please try again.',
    paymentMethodIsAlreadyRegisteredError: 'The "{title}" payment method is already registered. Please enter another title.',
  },
}
