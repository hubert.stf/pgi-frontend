export default {
  'pt-BR': {
    incomeIsAlreadyRegisteredError: 'A receita "{title}" já está cadastrada. Por favor, informe outro título.',
  },
  'en-US': {
    incomeIsAlreadyRegisteredError: 'The "{title}" income is already registered. Please enter another title.',
  },
}
