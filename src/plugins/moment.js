import moment from 'moment'

export const setLocale = locale => {
  moment.locale(locale)
}

setLocale(process.env.VUE_APP_DEFAULT_LOCALE || process.env.VUE_APP_FALLBACK_LOCALE)
