/*
* ----------------------------------------------------------------------
* NAME THAT COLOR lib added in <HEAD> tag of the /public/index.html.
* It is accessible from "window object" under the "ntc" abbreviation.
* ----------------------------------------------------------------------
* */

const NameThatColor = window.ntc
const { name } = NameThatColor

/*
* --------------------------------------------------
* -------------- TO ANYONE UNDERSTAND --------------
* --------------------------------------------------
* HEX   =   7 digits, like #000000 (#RGB)
* HEXA  =   9 digits, like #00000000 (#RGBT)
* R = Red / B = Blue / G = Green / T = Transparency
* --------------------------------------------------
* */

const isHexaSet = hexa => !!hexa
const isHexaCode = hexa => hexa.length === 9
const getHexCode = hex => hex.substr(0, 7)
const getColorName = hex => {
  const COLOR_NAME = 1
  return name(hex)[COLOR_NAME]
}

export const nameThatColor = hexa => {
  if (!isHexaSet(hexa)) {
    return
  }

  const hex = isHexaCode(hexa) ? getHexCode(hexa) : hexa
  return getColorName(hex)
}
