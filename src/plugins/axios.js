import Vue from 'vue'
import axios from 'axios'
import { cleanAuthUser } from '@/services/auth'
import { redirectToLogin } from '@/services/router'

import {
  UNAUTHORIZED,
  PAGE_EXPIRED,
} from '@/data/response-status-code'

const api = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  timeout: 30000,
  withCredentials: true,
  headers: {
    common: {
      'X-Requested-With': 'XMLHttpRequest',
    },
  },
})

api.interceptors.response.use(function (response) {
  return response
}, function (error) {
  switch (error.response.status) {
    case UNAUTHORIZED:
    case PAGE_EXPIRED:
      cleanAuthUser()
      redirectToLogin()
      break
    default:
      return Promise.reject(error)
  }
})

// Register 'api' globally in Vue instance.
Vue.prototype.$api = api

export default api
