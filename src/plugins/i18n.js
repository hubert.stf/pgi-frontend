import Vue from 'vue'
import VueI18n from 'vue-i18n'
import { ptBR, enUS } from '../translations'

Vue.use(VueI18n)

const numberFormats = {
  'pt-BR': {
    currency: {
      style: 'currency',
      currency: 'BRL',
      currencyDisplay: 'symbol',
    },
  },
  'en-US': {
    currency: {
      style: 'currency',
      currency: 'USD',
      currencyDisplay: 'symbol',
    },
  },
}

const i18n = new VueI18n({
  locale: process.env.VUE_APP_DEFAULT_LOCALE,
  fallbackLocale: process.env.VUE_APP_DEFAULT_LOCALE,
  silentTranslationWarn: true,
  messages: {
    'pt-BR': ptBR,
    'en-US': enUS,
  },
  numberFormats,
})

export const setLocale = locale => {
  i18n.locale = locale
}

export default i18n
