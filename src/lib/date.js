import moment from 'moment'

export const formatDateToRelativeTime = date => {
  if (!date) {
    return
  }

  // TO-DO: Use Moment Timezone to remove this .subtract('3', 'hours')
  return moment(date, 'YYYYMMDD h:mm:ss')
    .subtract('3', 'hours')
    .fromNow()
}

export const formatDate = date => {
  if (!date) {
    return
  }

  return moment(date, 'YYYYMMDD')
    .format('L')
}

export const formatMonth = yearAndMonth => {
  if (!yearAndMonth) {
    return
  }

  return moment(yearAndMonth, 'YYYYMM')
    .format('MM/YYYY')
}

export const isSameOrAfter = (startDate, endDate) => {
  return moment(endDate, 'YYYYMMDD')
    .isSameOrAfter(startDate)
}

export const getCurrentTimestamp = () => {
  return Date.now()
}

export const getCurrentRelativeTime = () => {
  return moment().fromNow()
}
