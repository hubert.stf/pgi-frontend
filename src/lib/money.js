import { i18n } from '@/plugins'

export const formatCurrency = amount => {
  if (!amount) {
    return
  }

  return i18n.n(amount, 'currency')
}
