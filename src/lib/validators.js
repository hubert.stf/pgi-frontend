import { i18n } from '@/plugins'
import * as v from 'vuelidate/lib/validators'
import { formatDate, isSameOrAfter } from '@/lib/date'

const getDefaultMessage = (key, params) => (
  i18n.t('global.validations.' + key, params)
)

export const required = (value, messageIfInvalid = getDefaultMessage('required')) =>
  v.required(value) || messageIfInvalid

export const minLength = (
  length, messageIfInvalid = getDefaultMessage('minLength', { n: length }),
) => value => (
  v.minLength(length)(value) || messageIfInvalid
)

export const maxLength = (
  length, messageIfInvalid = getDefaultMessage('maxLength', { n: length }),
) => value => (
  v.maxLength(length)(value) || messageIfInvalid
)

export const minDate = (
  minDate, messageIfInvalid = getDefaultMessage('minDate', { date: formatDate(minDate) }),
) => value => (
  isSameOrAfter(minDate, value) || messageIfInvalid
)

export const decimal = (value, messageIfInvalid = getDefaultMessage('decimal')) => {
  return v.decimal(value) || messageIfInvalid
}

export const maxDecimalLength = (
  maxLength, separator, messageIfInvalid = getDefaultMessage('maxDecimalLength', { n: maxLength }),
) => value => {
  if (!value) {
    return true
  }
  const intPart = value.split(separator)[0]
  return (intPart.length <= maxLength) || messageIfInvalid
}

export const maxDecimalPrecisionLength = (
  maxPrecisionLength,
  separator,
  messageIfInvalid = getDefaultMessage('maxDecimalPrecisionLength', { n: maxPrecisionLength }),
) => value => {
  if (!value) {
    return true
  }
  const decimalPart = value.split(separator)[1]
  return !decimalPart || (decimalPart.length <= maxPrecisionLength) || messageIfInvalid
}

export const integer = (value, messageIfInvalid = getDefaultMessage('integer')) => {
  return v.integer(value) || messageIfInvalid
}

export const minValue = (
  minValue, messageIfInvalid = getDefaultMessage('minValue', { n: minValue }),
) => value => {
  return v.minValue(minValue)(value) || messageIfInvalid
}

export const maxValue = (
  maxValue, messageIfInvalid = getDefaultMessage('maxValue', { n: maxValue }),
) => value => {
  return v.maxValue(maxValue)(value) || messageIfInvalid
}

export const equal = (
  number, messageIfInvalid = getDefaultMessage('equal', { n: number }),
) => value => {
  return number === value || messageIfInvalid
}

export const array = values => value => {
  return values.includes(value)
}
